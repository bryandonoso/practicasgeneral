package com.example.alexdonoso.primeraapp4c;

import android.app.Dialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    Button buttonLogin, buttonGuardar, buttonBuscar, buttonPasarParametro, buttonFragmento;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        buttonLogin = (Button) findViewById(R.id.buttonLogin);
        buttonGuardar = (Button) findViewById(R.id.buttonGuardar);
        buttonBuscar = (Button) findViewById(R.id.buttonBuscar);
        buttonPasarParametro = (Button) findViewById(R.id.buttonPasarParametro);
        buttonFragmento = (Button) findViewById(R.id.buttonFragmento);
        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this,
                        ActividadLogin.class);
                startActivity(intent);
            }
        });
        buttonBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this,
                        ActividadBuscar.class);
                startActivity(intent);
            }
        });
        buttonGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this,
                        ActividadGuardar.class);
                startActivity(intent);
            }
        });
        buttonPasarParametro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this,
                        ActividadPasarParametro.class);
                startActivity(intent);
            }
        });
        buttonFragmento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, Fragmentos.class);
                startActivity(intent);
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;

        switch (item.getItemId()){
            case R.id.opcionLogin:
                Dialog dialogoLogin = new Dialog(MainActivity.this);
                dialogoLogin.setContentView(R.layout.dig_log);
                dialogoLogin.show();
                break;
        }
        switch (item.getItemId()){
            case R.id.opcionRegistrar:
                intent = new Intent(MainActivity.this, ActividadLogin.class);
                startActivity(intent);
                break;
        }
        switch (item.getItemId()){
            case R.id.opcioneAcelerometro:
                intent = new Intent(MainActivity.this, SensorAcelerometro.class);
                startActivity(intent);
                break;
        }
        switch (item.getItemId()){
            case R.id.opcionBrujula:
                intent = new Intent(MainActivity.this, SensorBrujula.class);
                startActivity(intent);
                break;
        }
        switch (item.getItemId()){
            case R.id.opcionVibracion:
                intent = new Intent(MainActivity.this, Vibrador.class);
                startActivity(intent);
                break;
        }
        switch (item.getItemId()){
            case R.id.opcionProximidad:
                intent = new Intent(MainActivity.this, SensorProximidad.class);
                startActivity(intent);
                break;

        }

        return true;
    }
}

